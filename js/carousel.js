import { elements } from './base';
import { backgrounds, mobileBackgrounds } from './images';

let index = 0;

const text = [
    `We provide unmatched quality, comfort, and style for property owners across the country. Our experts combine form and function in bringing your vision to life. Create a room in your own style with our collection and make your property a reflection of you and what you love.`,
    `With stores all over the world, it's easy for you to find furniture for your home or place of business. Locally, we’re in most major cities throughout the country. Find the branch nearest you using our store locator. Any questions? Don't hesitate to contact us today.`,
    `Our modern furniture store provide a high level of quality. Our company has invested in advanced technology to ensure that every product is made as perfect and as consistent as possible. With three decades of experience in this industry, we understand what customers want for their home and office.`
]

const title = [
    'Discover innovative ways to decorate',
    'We are available all across the globe',
    'Manufactured with the best materials'
]

const loop = (arr) => {
    return index = (index + 1) % arr.length;
}

const backLoop = (arr) => {
    if (index <= 0) {
        index = arr.length - 1;
    } else {
        --index;
    }
}

const cycleImages = (img, title) => {
    loop(img);
    loop(title);

    elements.headerLeft.style.backgroundImage = `url(${img[index]})`;


    elements.headerTitle.textContent = title[index];
    elements.text.textContent = text[index];
}

const cycleImagesBack = (img, title) => {
    backLoop(img);
    backLoop(title);

    elements.headerLeft.style.backgroundImage = `url(${img[index]})`;


    elements.headerTitle.textContent = title[index];
    elements.text.textContent = text[index];
}



// clicking right arrow will cycle images forward
elements.rightArrow.addEventListener('click', () => {
    if (window.innerWidth < 960) {
        cycleImages(mobileBackgrounds, title);
    } else {
        cycleImages(backgrounds, title);
    }
})

// clicking left arrow will cycle images backward
elements.leftArrow.addEventListener('click', () => {
    if (window.innerWidth < 960) {
        cycleImagesBack(mobileBackgrounds, title);
    } else {
        cycleImagesBack(backgrounds, title);
    }
})

window.addEventListener('resize', () => {
    if (window.innerWidth < 991) {
        console.log('mobile');
        document.querySelector('.header__block-left').style.backgroundImage = `url(${mobileBackgrounds[index]})`;
    } else {
        console.log('desktop')
        document.querySelector('.header__block-left').style.backgroundImage = `url(${backgrounds[index]})`;

    }
});
