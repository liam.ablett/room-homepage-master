
import headerImage1 from '../images/desktop-image-hero-1.jpg';
import headerImage2 from '../images/desktop-image-hero-2.jpg';
import headerImage3 from '../images/desktop-image-hero-3.jpg';
import logo from '../images/logo.svg';
import slideArrowLeft from '../images/icon-angle-left.svg';
import slideArrowRight from '../images/icon-angle-right.svg';
import sectionLeftImage from '../images/image-about-dark.jpg';
import sectionRightImage from '../images/image-about-light.jpg';
import burgerMenu from '../images/icon-hamburger.svg';
import buttonArrow from '../images/icon-arrow.svg';
import iconClose from '../images/icon-close.svg';

import headerImageMobile1 from '../images/mobile-image-hero-1.jpg';
import headerImageMobile2 from '../images/mobile-image-hero-2.jpg';
import headerImageMobile3 from '../images/mobile-image-hero-3.jpg';

document.querySelector('.header__block-left').style.backgroundImage = `url(${headerImage1})`;


export const backgrounds = [
  headerImage1, headerImage2, headerImage3
]

export const mobileBackgrounds = [headerImageMobile1, headerImageMobile2, headerImageMobile3];

const images = {
  headerImageDesktop1: document.getElementById('desktopImageHero1'),
  headerImageDesktop1Mobile: document.getElementById('desktopImageHero1Mobile'),
  homeLogo: document.getElementById('homeLogo'),
  slideLeft: document.getElementById('slideArrowLeft'),
  slideRight: document.getElementById('slideArrowRight'),
  sectionLeft: document.getElementById('headerBlockSectionLeftImg'),
  sectionRight: document.getElementById('headerBlockSectionRightImg'),
  hamBurger: document.getElementById('burgerMenu'),
  arrow: document.getElementById('headerBlockButtonArrow'),
  closeIcon: document.getElementById('closeIcon'),

}

// images.headerImageDesktop1.src = headerImage1;
images.homeLogo.src = logo;
images.slideLeft.src = slideArrowLeft;
images.slideRight.src = slideArrowRight;
images.sectionLeft.src = sectionLeftImage;
images.sectionRight.src = sectionRightImage;
images.hamBurger.src = burgerMenu;
images.closeIcon.src = iconClose;
images.arrow.src = buttonArrow;


