
import '../scss/_style.scss';
import { elements } from './base';
import './carousel';


elements.burgerMenu.addEventListener('click', () => {
    if (!(elements.navList.classList.contains('nav__list-animate'))) {

        elements.navList.classList.add('nav__list-animate')
        elements.homeLogo.classList.add('hide-home-logo');
        elements.burgerMenu.classList.add('hide-burger');
        elements.closeIcon.classList.add('show-close');
        elements.navi.classList.add('navigation-style')
    }
})

elements.closeIcon.addEventListener('click', () => {
    elements.navList.classList.remove('nav__list-animate')
    elements.navList.classList.remove('show-nav');
    elements.homeLogo.classList.remove('hide-home-logo');
    elements.burgerMenu.classList.remove('hide-burger');
    elements.closeIcon.classList.remove('show-close')
    elements.navi.classList.remove('navigation-style')
})





