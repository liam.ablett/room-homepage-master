export const elements = {
    hello: document.querySelector('.header__block-slide-title'),
    rightArrow: document.querySelector('.arrow-right'),
    leftArrow: document.querySelector('.arrow-left'),
    headerLeft: document.querySelector('.header__block-left'),
    headerTitle: document.querySelector('.header__block-slide-title'),
    text: document.querySelector('.header__block-slide-text'),
    burgerMenu: document.querySelector('.burger-menu'),
    navList: document.querySelector('.nav__list'),
    homeLogo: document.querySelector('.home-logo'),
    closeIcon: document.querySelector('.close-icon'),
    navi: document.querySelector('.navigation')
}


